# Patbator Collection 

[![pipeline status](https://gitlab.com/patbator/php-collection/badges/master/pipeline.svg)](https://gitlab.com/patbator/php-collection/commits/master) 
[![coverage report](https://gitlab.com/patbator/php-collection/badges/master/coverage.svg)](https://gitlab.com/patbator/php-collection/commits/master)

A tribute to smalltalk collection protocol using PHP ArrayObject.


## Install

### With Composer

``` bash
$ composer require patbator/collection
```

### Manually

Clone or download repository.

Require the provided PSR-4 autoloader to use Collection object
```php
require_once '[path where you cloned or downloaded this library]/autoload.php';
```

## Usage

Let's do some magic ;)

```php
use Patbator\Collection\Collection;

(new Collection)->addWithOccurences('pharo', 15); // -> collection of 15 times repeated 'pharo'

$languages = new Collection(['lisp', 'pharo', 'php']); // -> collection containing 'lisp', 'pharo' and 'php'
$languages->add('ruby'); // -> collection containing 'lisp', 'pharo', 'php' and 'ruby'
$languages->addAll(new Collection(['python', 'typescript'])); // collection containing 'lisp', 'pharo', 'php', 'ruby', 'python' and 'typescript'
$languages->addIfNotPresent('php'); // -> will not repeat 'php' in collection

$languages->includes('ruby'); // -> true
$languages->allSatisfy(fn ($item) => is_string($item)); // -> true, they are all strings
$languages->anySatisfy(fn ($item) => is_int($item)); // -> false, there is no int
$languages->noneSatisfy(fn ($item) => 'x' == substr($item, 0, 1)); // -> true, none starts with 'x'

$languages->collect(fn ($item) => substr($item, 0, 1)); // -> new collection containing 'l', 'p', 'p', 'r', 'p' and 't'

$languages->detect(fn ($item) => 't' == substr($item, 0, 1)); // -> 'typescript'
$languages->detect(fn ($item) => 'x' == substr($item, 0, 1)); // -> null

$languages->detectMax(fn ($item) => strlen($item)); // -> 'typescript', longest word
$languages->detectMin(fn ($item) => strlen($item)); // -> 'php', shortest word
$languages->detectSum(fn ($item) => strlen($item)); // -> 32, cumulated length of each string

$languages->eachDo(function($item) {
  echo $item . ', ';
}); // -> echo : lisp, pharo, php, ruby, python, typescript,

$languages->injectInto('', function($value, $item) { return $value .= ' : '; }); // -> 'lisp : pharo : php : ruby : python : typescript : '

$selection = $languages->reject(fn ($item) => 'python' == $item); // -> new collection without 'python'

$selection = $languages->select(fn ($item) => 'p' == substr($item, 0, 1)); // -> new collection containing 'pharo' and 'php'

```

## Testing

```bash
composer run-script test
```


## License

Copyright (c) 2019 Patrick Barroca <patrick.barroca@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/gpl-3.0.html>.