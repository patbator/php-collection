<?php

namespace Patbator\Collection;

require __DIR__ . '/../vendor/autoload.php';

use PHPUnit\Framework\TestCase;


class CollectionSizeTest extends TestCase
{
  /** @test */
  public function emptyCollectionSizeShouldBeZero()
  {
    $this->assertEquals(0, (new Collection())->size());
  }


  /** @test */
  public function twoElementsCollectionSizeShouldBeTwo()
  {
    $this->assertEquals(2, (new Collection([5, 'a']))->size());
  }


  /** @test */
  public function emptyCollectionShouldBeEmpty()
  {
    $this->assertTrue((new Collection())->isEmpty());
  }


  /** @test */
  public function twoElementsCollectionShouldNotBeEmpty()
  {
    $this->assertFalse((new Collection([5, 'a']))->isEmpty());
  }
}



class CollectionAddingTest extends TestCase
{
  /** @test */
  public function addedElementShouldBeInResultingArray()
  {
    $this->assertEquals(['a'], (new Collection())->add('a')->getArrayCopy());
  }


  /** @test */
  public function addedCollectionShouldBeInResultingArray()
  {
    $other = new Collection(['b', 'c']);
    $this->assertEquals(
      ['a', 'b', 'c'],
      (new Collection(['a']))->addAll($other)->getArrayCopy()
    );
  }


  /** @test */
  public function addedThreeOccurencesShouldBeThreeTimesInResultingArray()
  {
    $this->assertEquals(
      ['a', 'a', 'a'],
      (new Collection())->addWithOccurences('a', 3)->getArrayCopy()
    );
  }


  /** @test */
  public function addNotPresentShouldBeInResultingArray()
  {
    $this->assertEquals(
      ['a', 'b'],
      (new Collection(['a']))->addIfNotPresent('b')->getArrayCopy()
    );
  }


  /** @test */
  public function addPresentShouldNotBeRepeatedInResultingArray()
  {
    $this->assertEquals(
      ['a', 'b'],
      (new Collection(['a', 'b']))->addIfNotPresent('b')->getArrayCopy()
    );
  }
}




class CollectionTestingTest extends TestCase
{
  /** @test */
  public function allStringCollectionShouldAllSatisfyIsString()
  {
    $this->assertTrue((new Collection(['a', 'b', 'c']))
      ->allSatisfy(function ($item) {
        return is_string($item);
      }));
  }


  /** @test */
  public function collectionWithStringsAndIntShouldNotAllSatisfyIsString()
  {
    $this->assertFalse((new Collection(['a', 'b', 8]))
      ->allSatisfy(function ($item) {
        return is_string($item);
      }));
  }


  /** @test */
  public function allStringCollectionShouldAnySatisfyIsString()
  {
    $this->assertTrue((new Collection(['a', 'b', 'c']))
      ->anySatisfy(function ($item) {
        return is_string($item);
      }));
  }


  /** @test */
  public function allStringCollectionShouldNotAnySatisfyIsInt()
  {
    $this->assertFalse((new Collection(['a', 'b', 'c']))
      ->anySatisfy(function ($item) {
        return is_int($item);
      }));
  }


  /** @test */
  public function collectionWithStringsAndIntShouldAnySatisfyIsString()
  {
    $this->assertTrue((new Collection(['a', 'b', 8]))
      ->anySatisfy(function ($item) {
        return is_string($item);
      }));
  }


  /** @test */
  public function collectionWithStringsAndIntShouldAnySatisfyIsInt()
  {
    $this->assertTrue((new Collection(['a', 'b', 8]))
      ->anySatisfy(function ($item) {
        return is_int($item);
      }));
  }


  /** @test */
  public function collectionWithStringsAndIntShouldNoneSatisfyIsObject()
  {
    $this->assertTrue((new Collection(['a', 'b', 8]))
      ->noneSatisfy(function ($item) {
        return is_object($item);
      }));
  }


  /** @test */
  public function collectionWithStringsAndIntShouldNotNoneSatisfyIsInt()
  {
    $this->assertFalse((new Collection(['a', 'b', 8]))
      ->noneSatisfy(function ($item) {
        return is_int($item);
      }));
  }
}




class CollectionTransformTest extends TestCase
{
  /** @test */
  public function shouldBeAbleToCollectIsString()
  {
    $this->assertEquals(
      [true, true, false, true, false, true],
      (new Collection(['a', 'b', 8, 'c', [], 'd']))
        ->collect(function ($item) {
          return is_string($item);
        })
        ->getArrayCopy()
    );
  }


  /** @test */
  public function shouldBeAbleToCollectIsStringIntoACollection()
  {
    $collection = new Collection;
    (new Collection(['a', 'b', 8, 'c', [], 'd']))
      ->collectInto(
        function ($item) {
          return is_string($item);
        },
        $collection
      );

    $this->assertEquals(
      [true, true, false, true, false, true],
      $collection->getArrayCopy()
    );
  }


  /** @test */
  public function shouldBeAbleToDetect8()
  {
    $this->assertEquals(
      8,
      (new Collection(['a', 'b', 8, 'c', [], 'd']))
        ->detect(function ($item) {
          return is_int($item);
        })
    );
  }


  /** @test */
  public function shouldBeAbleToFailDetection()
  {
    $this->assertNull((new Collection(['a', 'b', 8, 'c', [], 'd']))
      ->detect(function ($item) {
        return is_object($item);
      }));
  }


  /** @test */
  public function shouldBeAbleToDetectMax24()
  {
    $this->assertEquals(
      24,
      (new Collection([14, 8, 6, 24, 3, 9, 21]))
        ->detectMax(function ($item) {
          return $item;
        })
    );
  }


  /** @test */
  public function shouldBeAbleToDetectMin3()
  {
    $this->assertEquals(
      3,
      (new Collection([14, 8, 6, 24, 3, 9, 21]))
        ->detectMin(function ($item) {
          return $item;
        })
    );
  }


  /** @test */
  public function shouldBeAbleToDetectSum3()
  {
    $this->assertEquals(
      3,
      (new Collection([1, 1, 0, 1]))
        ->detectSum(function ($item) {
          return $item;
        })
    );
  }


  /** @test */
  public function shouldBeAbleToInjectIntoAString()
  {
    $this->assertEquals(
      'abc',
      (new Collection(['a', 'b', 'c']))
        ->injectInto('', function ($value, $item) {
          return $value .= $item;
        })
    );
  }



  /** @test */
  public function shouldBeAbleToSelectStrings()
  {
    $this->assertEquals(
      ['a', 'b', 'c', 'd'],
      (new Collection(['a', 'b', 8, 'c', [], 'd']))
        ->select(function ($item) {
          return is_string($item);
        })
        ->getArrayCopy()
    );
  }


  /** @test */
  public function shouldBeAbleToRejectStrings()
  {
    $this->assertEquals(
      [8, []],
      (new Collection(['a', 'b', 8, 'c', [], 'd']))
        ->reject(function ($item) {
          return is_string($item);
        })
        ->getArrayCopy()
    );
  }
}
