<?php

/**
 * Patbator Collection, A tribute to smalltalk collection protocol using PHP ArrayObject.
 *
 * Copyright (c) 2019 Patrick Barroca <patrick.barroca@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/agpl-3.0.html>.
 */

namespace Patbator\Collection;

use Closure;

class Collection extends \ArrayObject
{
  public function size(): int
  {
    return $this->count();
  }


  public function isEmpty(): bool
  {
    return 0 == $this->count();
  }


  public function add($anObject): self
  {
    $this->append($anObject);
    return $this;
  }


  public function addAll(Collection $aCollection): self
  {
    $aCollection->eachDo(function ($each) {
      $this->add($each);
    });
    return $this;
  }


  public function addWithOccurences($anObject, int $anInteger): self
  {
    foreach (range(1, $anInteger) as $step)
      $this->add($anObject);

    return $this;
  }


  public function addIfNotPresent($anObject): self
  {
    if (!$this->includes($anObject))
      $this->add($anObject);

    return $this;
  }


  public function allSatisfy(Closure $aClosure): bool
  {
    $satisfy = true;
    $this->eachDo(function ($each) use ($aClosure, &$satisfy) {
      $satisfy = $satisfy && $aClosure($each);
    });

    return $satisfy;
  }


  public function anySatisfy(Closure $aClosure): bool
  {
    $satisfy = false;
    $this->eachDo(function ($each) use ($aClosure, &$satisfy) {
      $satisfy = $satisfy || $aClosure($each);
    });

    return $satisfy;
  }


  public function noneSatisfy(Closure $aClosure): bool
  {
    return !$this->anySatisfy($aClosure);
  }


  public function collect(Closure $aClosure)
  {
    return $this->collectInto($aClosure, new self([]));
  }


  public function collectInto(Closure $aClosure, Collection $aCollection): self
  {
    return $aCollection->fillFromWith($this, $aClosure);
  }


  public function fillFromWith(Collection $aCollection, Closure $aClosure): self
  {
    $aCollection->eachDo(fn ($each) => $this->add($aClosure($each)));

    return $this;
  }


  public function detect(Closure $aClosure)
  {
    foreach ($this as $each)
      if ($aClosure($each))
        return $each;
  }


  public function detectMax(Closure $aClosure)
  {
    $maxElement = new MaxElement;
    $this->eachDo(fn ($each) => $maxElement->receive($each, $aClosure));

    return $maxElement->element();
  }


  public function detectMin(Closure $aClosure)
  {
    $minElement = new MinElement;
    $this->eachDo(fn ($each) => $minElement->receive($each, $aClosure));

    return $minElement->element();
  }


  public function detectSum($aClosure): int
  {
    $sum = 0;
    $this->eachDo(function ($each) use ($aClosure, &$sum) {
      $sum += $aClosure($each);
    });

    return $sum;
  }


  public function eachDo($aClosure): self
  {
    foreach ($this as $each)
      $aClosure($each);

    return $this;
  }


  public function injectInto($value, $aClosure)
  {
    $nextValue = $value;
    $this->eachDo(function ($each) use (&$nextValue, $aClosure) {
      $nextValue = $aClosure($nextValue, $each);
    });

    return $nextValue;
  }


  public function reject($aClosure): self
  {
    return $this->select(fn ($element) => !$aClosure($element));
  }


  public function select($aClosure): self
  {
    $newCollection = new static([]);
    $this->eachDo(function ($each) use ($aClosure, $newCollection) {
      if ($aClosure($each))
        $newCollection->add($each);
    });

    return $newCollection;
  }


  public function includes($anElement): bool
  {
    return $this->anySatisfy(function ($each) use ($anElement) {
      return $each == $anElement;
    });
  }
}




abstract class CompareElement
{
  protected $_value, $_element;

  public function element()
  {
    return $this->_element;
  }


  public function receive($anElement, Closure $aClosure): self
  {
    $compare_value = $aClosure($anElement);
    if (!isset($this->_value) || $this->_shouldSwap($compare_value))
      $this->_swapWith($anElement, $compare_value);

    return $this;
  }


  protected function _swapWith($anElement, $aMaxValue): self
  {
    $this->_value = $aMaxValue;
    $this->_element = $anElement;

    return $this;
  }


  abstract protected function _shouldSwap($compare_value): bool;
}




class MaxElement extends CompareElement
{
  protected function _shouldSwap($compare_value): bool
  {
    return $compare_value > $this->_value;
  }
}




class MinElement extends CompareElement
{
  protected function _shouldSwap($compare_value): bool
  {
    return $compare_value < $this->_value;
  }
}
